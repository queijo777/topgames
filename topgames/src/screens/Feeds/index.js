import React, { createRef } from 'react'
import { FlatList, View, Text } from 'react-native'
import { Header, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/AntDesign'
import { DrawerLayout } from 'react-native-gesture-handler'
import FeedCard from '../../components/FeedCard'
import {
  EntradaNomeProduto,
  CentralizadoNaMesmaLinha,
  ContenedorMensagem,
  Mensagem,
  Espacador
} from '../../assets/styles'
import Menu from '../../components/Menu'
import { getFeeds, getFeedsPorProduto, getFeedsPorEmpresa, feedsAlive } from '../../api'

export default class Feeds extends React.Component {
  constructor(props) {
    super(props)

    this.filtrarPorEmpresa = this.filtrarPorEmpresa.bind(this)
    this.menuRef = createRef()

    this.state = {
      proximaPagina: 1,
      feeds: [],

      empresaEscolhida: null,
      nomeProduto: null,
      podeVerFeeds: true,
      atualizando: false,
      carregando: false,
    }
  }

  mostrarMaisFeeds = (maisFeeds) => {
    const {proximaPagina, feeds} = this.state

    if (maisFeeds.length) {
      console.log('Adicionando ' + maisFeeds.length + ' feeds')

      this.setState({
        proximaPagina: proximaPagina + 1,
        feeds: [...feeds, ...maisFeeds],

        atualizando: false,
        carregando: false,
      })
    } else {
      this.setState({
        atualizando: false,
        carregando: false,
      })
    }
  }

  carregarFeeds = () => {
    const { proximaPagina, nomeProduto , empresaEscolhida } = this.state

    this.setState({
      carregando: true,
    })

    feedsAlive().then((resultado) => {
      if (resultado.alive === "yes") {
          this.setState({
            podeVerFeeds: true
          }, () => {
            if (empresaEscolhida) {
              getFeedsPorEmpresa(empresaEscolhida._id, proximaPagina).then((maisFeeds) => {
                this.mostrarMaisFeeds(maisFeeds)
              }).catch((erro) => {
                console.log("Erro ao filtrar por empresa: " + erro)
              })
            }
            else if (nomeProduto) {
              getFeedsPorProduto(nomeProduto, proximaPagina).then((maisFeeds) => {
                this.mostrarMaisFeeds(maisFeeds)
              }).catch((erro) => {
                console.error("Erro ao acessar feeds: " + erro)
              })
            } else {
              getFeeds(proximaPagina).then((maisFeeds) => {
                this.mostrarMaisFeeds(maisFeeds)
              }).catch((erro) => {
                console.error("Erro ao acessar feeds: " + erro)
              })
            }
          }
        )
      } else {
        this.setState({
          podeVerFeeds: false
        })
      }
    }).catch((erro) => {
      console.log("Erro ao verificar disponibilidade do serviço: " + erro)
    })
  }

  componentDidMount = () => {
    this.carregarMaisFeeds()
  }

  carregarMaisFeeds = () => {
    const { carregando } = this.state

    if (carregando) {
      return
    }

    this.carregarFeeds()
  }

  atualizar = () => {
    this.setState(
      { atualizando: true, feeds: [], proximaPagina: 1, nomeProduto: null, empresaEscolhida: null },
      () => {
        this.carregarFeeds()
      }
    )
  }

  atualizarNomeProduto = nome => {
    this.setState({
      nomeProduto: nome,
    })
  }

  filtrarPorEmpresa = (empresa) => {
    this.setState({
      empresaEscolhida: empresa,
      proximaPagina: 1,
      feeds: []
    }, ()  => {
      this.carregarFeeds()
    })

    console.log("Filtrar por empresa Menu: " + this.menuRef)
    this.menuRef.current.closeDrawer()
  }

  mostrarFeed = feed => {
    return <FeedCard feed={feed} navegador={this.props.navigation} />
  }

  mostrarBarraPesquisa = () => {
    const { nomeProduto } = this.state

    return (
      <CentralizadoNaMesmaLinha>
        <EntradaNomeProduto
          onChangeText={nome => {
            this.atualizarNomeProduto(nome)
          }}
          value={nomeProduto}
        ></EntradaNomeProduto>
        <Icon
          style={{ padding: 16 }}
          size={24}
          name="search1"
          onPress={
            () => this.setState({
              proximaPagina: 1,
              feeds: []
            }, () => this.carregarFeeds())
          }
        ></Icon>
      </CentralizadoNaMesmaLinha>
    )
  }

  mostrarMenu = () => {
    this.menuRef.current.openDrawer()
  }

  mostrarFeeds = feeds => {
    const { atualizando } = this.state

    return (
      <DrawerLayout
        drawerWidth = {250}
        drawerPosition = {DrawerLayout.positions.Left}

        ref={this.menuRef}

        renderNavigationView={() => <Menu filtragem={this.filtrarPorEmpresa}/>}
      >
        <Header
          leftComponent={<Icon size={33} name="menuunfold" style={{paddingTop : 10}} onPress={() => {
            this.mostrarMenu()
          }}/>}
          centerComponent={this.mostrarBarraPesquisa()}
          rightComponent={<></>}
        ></Header>
        {!feeds.length ? <View><Text>Não há resultados para essa pesquisa</Text></View> : null}
        <FlatList
          data={feeds}
          numColumns={2}
          onEndReached={() => this.carregarMaisFeeds()}
          onEndReachedThreshold={0.1}
          onRefresh={() => this.atualizar()}
          refreshing={atualizando}
          keyExtractor={item => String(item._id)}
          renderItem={({ item }) => {
            return (
              <View style={{ width: '50%' }}>{this.mostrarFeed(item)}</View>
            )
          }}
        ></FlatList>
      </DrawerLayout>
    )
  }

  mostrarBotaoAtualizar = () => {
    return (
      <ContenedorMensagem>
        <Mensagem>Um dos nossos serviços não está funcionando :(</Mensagem>
        <Mensagem>Tente novamente mais tarde!</Mensagem>
        <Espacador/>
        <Button 
          icon = {
            <Icon 
              name = "reload1"
              size = {22}
              color = "#40e0d0"
            />
          }

          title = "Atualizar"
          type = "solid"

          onPress = {
            () => {
              this.carregarFeeds()
            }
          }
        />
      </ContenedorMensagem>
    )
  }

  mostrarMensagemCarregando = () => {
    return (
      <ContenedorMensagem>
        <Mensagem>Carregando feeds. Aguarde...</Mensagem>
      </ContenedorMensagem>
    )
  }

  render = () => {
    const { feeds, podeVerFeeds } = this.state

    if (podeVerFeeds) {
      if (feeds.length) {
        return (
          this.mostrarFeeds(feeds)
        )
      } else {
        return (
          this.mostrarMensagemCarregando()
        )
      }
    } else {
      return (
        this.mostrarBotaoAtualizar()
      )
    }
  }
}
