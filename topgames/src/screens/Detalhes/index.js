import React from "react";
import { View } from "react-native";
import { Header } from "react-native-elements";

import { SliderBox } from "react-native-image-slider-box";
import CardView from "react-native-cardview";
import Icon from "react-native-vector-icons/AntDesign";
import SyncStorage from "sync-storage";
import Toast from "react-native-simple-toast";

import {
    Avatar, 
    NomeProduto,
    DescricaoProduto,
    PrecoProduto,
    Likes,
    NomeEmpresa,
    CentralizadoNaMesmaLinha,
    EsquerdaDaMesmaLinha,
    Espacador, 
    Cabecalho
} from "../../assets/styles";
import Compartilhador from "../../components/Compartilhador";
import { getFeed, getImagem, usuarioGostou, gostar, desgostar, likesAlive, comentariosAlive } from '../../api'

const TOTAL_IMAGENS_SLIDE = 3

export default class Detalhes extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      feedId: this.props.navigation.state.params.feedId,
      feed: null,

      gostou: false,
      podeGostar: false,
      podeComentar: false
    }
  }

  verificarUsuarioGostou = () => {
    const { feedId } = this.state
    
    usuarioGostou(feedId).then((resultado) => {
      this.setState({
        gostou: (resultado.likes > 0)
      })
    }).catch((erro) => {
      console.log("Erro ao verificar like: " + erro)
    })
  }

  carregarFeed = () => {
    const { feedId } = this.state

    getFeed(feedId).then((feed) => {
      this.setState({
        feed: feed
      }, () => {
        this.verificarUsuarioGostou()
      })
    }).catch((erro) => {
      console.log("Houve um erro ao carregar o feed: " + erro)
    })
  }

  componentDidMount = () => {
    likesAlive().then((resultado) => {
      if (resultado.alive === "yes") {
        this.setState({
          podeGostar: true
        })
      } else {
        this.setState({
          podeGostar: false
        })
      }
    }).catch((erro) => {
      console.log("Erro ao verificar disponibilidade do serviço de likes: " + erro)
    })

    comentariosAlive().then((resultado) => {
      if (resultado.alive === "yes") {
        this.setState({
          podeComentar: true
        })
      } else {
        this.setState({
          podeComentar: false
        })
      }
    }).catch((erro) => {
      console.log("Erro ao verificar disponibilidade do serviço de comentários: " + erro)
    })

    this.carregarFeed()
  }

  mostrarSlides = () => {
    const { feed } = this.state
    let slides = []

    for (let i = 0; i < TOTAL_IMAGENS_SLIDE; i++) {
      if (feed.product.blobs[i].file) {
        slides = [...slides, getImagem(feed.product.blobs[i].file)]
      } 
    }

    return (
      <SliderBox
        dotColor={'#40e0d0'}
        inactiveDotColor={'#00ced1'}
        resizeMethod={'resize'}
        resizeMode={'cover'}
        dotStyle={{
          width: 15,
          height: 15,

          borderRadius: 15,
          marginHorizontal: 5,
        }}
        images={slides}
      />
    )
  }

  like = () => {
    const { feedId, podeGostar } = this.state
    
    gostar(feedId).then((resultado) => {
      if (podeGostar) {
        if (resultado.situacao === "ok") {
          this.carregarFeed()
          Toast.show("Obrigado pela sua avaliação", Toast.LONG)
        } else {
          Toast.show("Erro ao adcionar like", Toast.LONG)
        }
      } else {
        Toast.show("Serviço indisponível no momento, tente novamente mais tarde!", Toast.LONG)
      }
    }).catch((erro) => {
      Toast.show("Ocorreu um erro ao realizar a avaliação: " + erro, Toast.LONG)
    })
  }

  dislike = () => {
    const { feedId, podeGostar } = this.state
    
    desgostar(feedId).then((resultado) => {
      if (podeGostar) {
        if (resultado.situacao === "ok") {
          this.carregarFeed()
          Toast.show("Like removido", Toast.SHORT)
        }
      } else {
        Toast.show("Serviço indisponível no momento, tente novamente mais tarde!", Toast.LONG)
      }
    }).catch((erro) => {
      Toast.show("Ocorreu um erro ao remover like " + erro, Toast.LONG)
    })
  }

  render = () => {
    const { feed, gostou, podeGostar, podeComentar } = this.state
    
    if (feed) {
      return (
        <>
          <Header
            leftComponent={
              <Icon size={28} name="left" onPress={() => {this.props.navigation.goBack()}}/>
            }

            centerComponent={
              <>
                <CentralizadoNaMesmaLinha>
                  <Avatar source={getImagem(feed.company.avatar)}/>
                  <NomeEmpresa>{feed.company.name}</NomeEmpresa>
                </CentralizadoNaMesmaLinha>
              </>
            }

            rightComponent={
              <CentralizadoNaMesmaLinha>
                <Compartilhador feed={feed}/>
                <Espacador/>
                {gostou && <Icon name="heart" size={28} color={"#ff0000"} 
                  style={{opacity: podeGostar ? 1 : 0.5}} 
                  onPress={
                    () => {
                      this.dislike();  
                    }
                  }
                />}
                {!gostou && <Icon name="hearto" size={28} color={"#ff0000"}
                  style={{opacity: podeGostar ? 1 : 0.5}} 
                  onPress={  
                    () => {
                      this.like();  
                    }
                  }
                />}
              </CentralizadoNaMesmaLinha>
            }
          />
          <CardView cardElevation={2} cornerRadius={0}>
            {this.mostrarSlides()}
            <View style={{padding: 10}}>
              <NomeProduto>{feed.product.name}</NomeProduto>
              <DescricaoProduto>{feed.product.description}</DescricaoProduto>
              <Espacador/>
              <EsquerdaDaMesmaLinha>
                <PrecoProduto>{"R$ " + feed.product.price}  </PrecoProduto>
                <Icon name="heart" size={18} color={"#ff0000"}>
                  <Likes>  {feed.likes}</Likes>
                </Icon>
                <Espacador />
                <Icon name="message1" size={18} 
                  style={{opacity: podeComentar ? 1 : 0.5}} 
                  onPress={
                    () => {
                        if (podeComentar) { 
                          this.props.navigation.navigate("Comentarios",
                              { feedId: feed._id,
                                  empresa: feed.company,
                                  produto: feed.product })
                        } else {
                          Toast.show("Serviço indisponível no momento, tente novamente mais tarde!", Toast.LONG)
                        }
                    }
                }/>
              </EsquerdaDaMesmaLinha>
              <Espacador/>
            </View>
          </CardView>
        </>
      )
    } else {
      return null
    }
  }
}
