import React from 'react'
import { TouchableOpacity } from 'react-native'
import { Card, CardContent, CardImage } from 'react-native-cards'
import Icon from 'react-native-vector-icons/AntDesign'
import {
  Avatar,
  NomeEmpresa,
  NomeProduto,
  DescricaoProduto,
  PrecoProduto,
  Likes,
  EsquerdaDaMesmaLinha
} from '../../assets/styles'
import { getImagem } from '../../api'

export default class FeedCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      feed: this.props.feed,
      navegador: this.props.navegador,
    }
  }

  render = () => {
    const { feed, navegador } = this.state

    productImage = getImagem(feed.product.blobs[0].file)
    companyImage = getImagem(feed.company.avatar)

    return (
      <TouchableOpacity
        onPress={() => {
          navegador.navigate('Detalhes', { feedId: feed._id })
        }}
      >
        <Card>
          <CardImage source={productImage} />
          <CardContent>
            <EsquerdaDaMesmaLinha>
              <Avatar source={companyImage} />
              <NomeEmpresa>{feed.company.name}</NomeEmpresa>
            </EsquerdaDaMesmaLinha>
          </CardContent>
          <CardContent>
            <NomeProduto>{feed.product.name}</NomeProduto>
          </CardContent>
          <CardContent>
            <DescricaoProduto>{feed.product.description}</DescricaoProduto>
          </CardContent>
          <CardContent>
            <EsquerdaDaMesmaLinha>
              <PrecoProduto>{'R$ ' + feed.product.price}   </PrecoProduto>
              <Icon name="heart" size={18} color={"#ff0000"}>
                <Likes>  {feed.likes}</Likes>
              </Icon>
            </EsquerdaDaMesmaLinha>
          </CardContent>
        </Card>
      </TouchableOpacity>
    )
  }
}
