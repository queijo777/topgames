import React from 'react'
import { ScrollView, TouchableOpacity, View } from 'react-native'
import Toast from "react-native-simple-toast"

import {
    Avatar,
    NomeEmpresa,
    ContenedorMenu,
    EsquerdaDaMesmaLinha,
    DivisorMenu,
} from '../../assets/styles'
import { LoginOptionsMenu  } from '../../components/Login'
import { SafeAreaInsetsContext } from 'react-native-safe-area-context'
import { getImagem } from '../../api'

export default class Menu extends React.Component {

    constructor(props){
        super(props)

        this.state = {
            atualizar: true,
            filtrar: props.filtragem,

            empresas: []
        }
    }

    componentDidMount = async () => {
        try {
            const result = await fetch("http://192.168.0.119:5004/empresas")
            const empresas = await result.json()
            this.setState({
                empresas: empresas
            })
        } catch (error) {
            console.log("Erro ao carregar empresas: " + error)
        }
    }

    mostrarEmpresa = (empresa) => {
        const { filtrar } = this.state;
        
        return (
            <TouchableOpacity
                key = {empresa._id}
                onPress = { 
                    () => filtrar(empresa)
                }
                >
                <EsquerdaDaMesmaLinha>
                    <Avatar source={getImagem(empresa.avatar)}/>
                    <NomeEmpresa>{empresa.name}</NomeEmpresa>
                </EsquerdaDaMesmaLinha>
                <DivisorMenu/>
            </TouchableOpacity>
        )
    }

    onLogin = (usuario) => {
        this.setState({
            atualizar : true
        }, () => {
            console.log("Usuario logado: " + usuario.name)
            Toast.show("Você foi logado com sucesso em sua conta " + usuario.signer + "!", Toast.LONG)
        }) 
    }

    onLogout = (signer) => {
        this.setState({
            atualizar : true
        }, () => {
            Toast.show("Você foi deslogado com sucesso em sua conta " + signer + "!")
        })   
    }

    render = () => {
        const {empresas} = this.state;

        if (empresas.length){
            return(
                <SafeAreaInsetsContext.Consumer>
                    {insets => 
                        <ScrollView style={{ paddingTop: insets.top }}>
                            <LoginOptionsMenu onLogin={ this.onLogin } onLogout={ this.onLogout }/>
                            <ContenedorMenu>
                                {empresas.map((empresa) => this.mostrarEmpresa(empresa))}
                            </ContenedorMenu>
                        </ScrollView>
                    }
                </SafeAreaInsetsContext.Consumer>
            )
        }else {
            return (
                <View></View>
            )
        }
    }
}