import SyncStorage from 'sync-storage'

const FEEDS_URL = "http://192.168.0.119:5000/"
const EMPRESAS_URL = "http://192.168.0.119:5004/"
const COMENTARIOS_URL = "http://192.168.0.119:5002/"
const LIKES_URL = "http://192.168.0.119:5003/"
const ARQUIVOS_URL = "http://192.168.0.119:80/"

export const acessarUrl = async (url) => {
    let promise = null

    try {
        resposta = await fetch(url, {method: "GET"})

        if (resposta.ok) {
            promise = Promise.resolve(resposta.json())
        } else {
            promise = Promise.reject(resposta)
        }
    } catch (erro) {
        promise = Promise.reject(erro)
    }

    return promise
}

export const getFeeds = async (pagina) => {
    return acessarUrl(FEEDS_URL + "feeds/" + pagina)
}

export const getFeed = async (feedId) => {
    return acessarUrl(FEEDS_URL + "feed/" + feedId)
}

export const getFeedsPorProduto = async (nomeProduto, pagina) => {
    return acessarUrl(FEEDS_URL + "feeds_por_produto/" + nomeProduto + "/" + pagina)
}

export const getFeedsPorEmpresa = async (empresaId, pagina) => {
    return acessarUrl(FEEDS_URL + "feeds_por_empresa/" + empresaId + "/" + pagina)
}

export const getEmpresas = async () => {
    return acessarUrl(EMPRESAS_URL + "empresas")
}

export const usuarioGostou = async (feedId) => {
    let promise = null
    
    const usuario = SyncStorage.get("user")

    if (usuario) {
        promise =  acessarUrl(LIKES_URL + "gostou/" + usuario.acount + "/" + feedId)
    }

    return promise
}

export const gostar = async (feedId) => {
    let promise = null
    
    const usuario = SyncStorage.get("user")

    if (usuario) {
        promise =  acessarUrl(LIKES_URL + "gostar/" + usuario.acount + "/"  + feedId)
    }

    return promise
}

export const desgostar = async (feedId) => {
    let promise = null
    
    const usuario = SyncStorage.get("user")

    if (usuario) {
        promise =  acessarUrl(LIKES_URL + "desgostar/" + usuario.acount + "/"  + feedId)
    }

    return promise
}

export const getComentarios = async (feedId, pagina) => {
    return acessarUrl(COMENTARIOS_URL + "comentarios/" + feedId + "/" + pagina)
}

export const adicionarComentario = async (feedId, comentario) => {
    let promise = null
    
    const usuario = SyncStorage.get("user")

    if (usuario) {
        console.log("Adicionando comentário: " + comentario + " em " + feedId + " de " + usuario.name)
        promise = acessarUrl(COMENTARIOS_URL + "adicionar/" + feedId + "/" + usuario.name + "/" + usuario.account
            + "/" + comentario)
    }

    return promise
}

export const removerComentario = async (comentarioId) => {
    return acessarUrl(COMENTARIOS_URL + "remover/" + comentarioId)
}

export const feedsAlive = async () => {
    return acessarUrl(FEEDS_URL + "isalive")
}

export const comentariosAlive = async () => {
    return acessarUrl(COMENTARIOS_URL + "isalive")
}

export const empresasAlive = async () => {
    return acessarUrl(EMPRESAS_URL + "isalive")
}

export const likesAlive = async () => {
    return acessarUrl(LIKES_URL + "isalive")
}

export const getImagem = (imagem) => {
    return { uri: ARQUIVOS_URL + imagem }
}